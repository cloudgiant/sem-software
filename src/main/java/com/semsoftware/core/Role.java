package com.semsoftware.core;

/**
 * 
 * @author ozdesimsek
 *
 */
public enum Role {
  ADMIN, USER
}
