package com.semsoftware.core.repository;

import com.semsoftware.core.model.Invoice;;

public interface InvoiceRepository extends GenericRepository<Invoice>{

}
