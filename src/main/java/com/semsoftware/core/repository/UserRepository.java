package com.semsoftware.core.repository;

import java.util.List;

import com.semsoftware.core.dto.UserDTO;
import com.semsoftware.core.model.User;


public interface UserRepository extends GenericRepository<User>{

  List<UserDTO> findAllDTOs();

  UserDTO findUserByEmail(String email);

  List<User> findUsers(String username, String email);

  List<UserDTO> findAllAdmins();

  User findUser(String emailUsername);

}
