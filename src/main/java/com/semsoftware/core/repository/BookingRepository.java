package com.semsoftware.core.repository;

import com.semsoftware.core.dto.BookingDTO;
import com.semsoftware.core.dto.ClientDTO;
import com.semsoftware.core.dto.RoomDTO;
import com.semsoftware.core.model.Booking;

import java.util.Date;
import java.util.List;

public interface BookingRepository extends GenericRepository<Booking>{
  
  public List<BookingDTO> findAllBookingDTOs();

  public List<ClientDTO> findAllClientsForBooking(Integer id);

  public RoomDTO findRoomForBooking(Integer id);

  public RoomDTO findBookedRoomForClient(ClientDTO client);

  public List<BookingDTO> findAllBookingsOnDate(Date date);

  public List<BookingDTO> findAllBookingsForClient(ClientDTO client);

  public List<BookingDTO> findAllBookingsInPeriod(Date start, Date end);

  public List<BookingDTO> findAllBookingsWithDiscount();

  public BookingDTO findBookingOnId(Integer id);
  
  public List<BookingDTO> findCanceledBookings();
  
  public List<BookingDTO> findActiveBookings();
  
  public List<BookingDTO> findBookingsBetweenDates(Date from, Date to);


}
