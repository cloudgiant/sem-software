package com.semsoftware.core.repository;

import com.semsoftware.core.dto.BookingDTO;
import com.semsoftware.core.dto.RoomDTO;
import com.semsoftware.core.model.Booking;

import java.util.List;

public interface RoomRepository extends GenericRepository<RoomDTO>{
  
  public List<RoomDTO> findAllRoomDTOs();
}