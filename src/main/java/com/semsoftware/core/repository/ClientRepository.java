package com.semsoftware.core.repository;

import com.semsoftware.core.dto.ClientDTO;
import com.semsoftware.core.model.Client;

import java.util.List;

public interface ClientRepository extends GenericRepository<Client>{

  public List<ClientDTO> getAllClientDTOs();

}
