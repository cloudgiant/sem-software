package com.semsoftware.core.repository;

import com.semsoftware.core.dto.LogDTO;

import java.util.List;

/**
 * Created by Martina on 06.01.2016.
 */
public interface LogRepository {

    public List<LogDTO> findAllLogs();
}
