package com.semsoftware.core.repository;

import java.util.List;

/**
 * 
 * @author ozdesimsek
 * @param <T>
 *
 */
public interface GenericRepository<T> {
  
  T findById(Integer id);

  List<T> findAll();

  T save(T entity);

  void merge(T entity);

  void delete(T entity);

  void flush();

}
