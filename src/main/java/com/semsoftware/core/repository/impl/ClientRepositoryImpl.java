package com.semsoftware.core.repository.impl;

import com.semsoftware.core.dto.ClientDTO;
import com.semsoftware.core.model.Client;
import com.semsoftware.core.repository.ClientRepository;
import com.semsoftware.core.util.Constants;

import javax.inject.Named;
import javax.persistence.Query;
import java.util.List;

@Named
public class ClientRepositoryImpl extends GenericRepositoryImpl<Client> implements ClientRepository {

  @Override
  public List<ClientDTO> getAllClientDTOs() {
    Query query = em.createNamedQuery(Constants.QUERY_ALL_CLIENTS);
    return query.getResultList();
  }

}
