package com.semsoftware.core.repository.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Query;

import com.semsoftware.core.dto.UserDTO;
import com.semsoftware.core.model.User;
import com.semsoftware.core.repository.UserRepository;
import com.semsoftware.core.service.EntityConverter;
import com.semsoftware.core.util.Constants;

@Named
public class UserRepositoryImpl extends GenericRepositoryImpl<User> implements UserRepository {

  @Inject
  private EntityConverter entityConverter;
  
  @Override
  public List<UserDTO> findAllDTOs() {
    final Query query = em.createNamedQuery(Constants.FIND_ALL_USERS);

    return query.getResultList();
  }

  @Override
  public UserDTO findUserByEmail(String email) {
    final Query query = em.createNamedQuery(Constants.QUERY_FIND_USER_BY_EMAIL);
    query.setParameter("e_mail", email);
    final List<UserDTO> result = query.getResultList();
    return !result.isEmpty() ? result.get(0) : null;
  }

  @Override
  public List<User> findUsers(String username, String email) {
    final Query query = em.createNamedQuery(Constants.QUERY_FIND_USER_NAME_EMAIL);
    query.setParameter("username", username);
    query.setParameter("e_mail", email);
    return query.getResultList();
  }

  @Override
  public List<UserDTO> findAllAdmins() {
    Query query = em.createNamedQuery(Constants.QUERY_ALL_ADMINS);
    return query.getResultList();
  }

  @Override
  public User findUser(String emailUsername) {
    final Query query = em.createNamedQuery(Constants.QUERY_FIND_USER_NAME_EMAIL);
    query.setParameter("username", emailUsername);
    query.setParameter("e_mail", emailUsername);
    final List<UserDTO> list=query.getResultList();
    return !list.isEmpty() ? entityConverter.createUser((UserDTO) list.get(0)) : null; 
  }

}
