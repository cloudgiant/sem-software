package com.semsoftware.core.repository.impl;

import com.semsoftware.core.dto.LogDTO;
import com.semsoftware.core.model.Log;
import com.semsoftware.core.repository.LogRepository;
import com.semsoftware.jdbc.JDBCConnection;

import javax.inject.Named;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Martina on 06.01.2016.
 */
@Named
public class LogRepositoryImpl extends GenericRepositoryImpl<Log> implements LogRepository {
    private Connection conn;
    private JDBCConnection jdbcConnection;

    @Override
    public List<LogDTO> findAllLogs() {

        Statement st;
        ResultSet rs;
        List<LogDTO> list = new ArrayList<>();

        this.jdbcConnection = new JDBCConnection();
        try {
            this.conn = this.jdbcConnection.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        try {
            st = this.conn.createStatement();
            rs = st.executeQuery("select * from logs");

            while (rs.next()) {
                LogDTO log = new LogDTO(rs.getString("user_id"), new Date(rs.getTimestamp("date").getTime()), rs.getString("logger"), rs.getString("level"), rs.getString("message"));
                list.add(log);
            }
            rs.close();
            st.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

}
