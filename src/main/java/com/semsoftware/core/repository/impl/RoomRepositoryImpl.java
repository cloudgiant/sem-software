package com.semsoftware.core.repository.impl;


import com.semsoftware.core.dto.RoomDTO;
import com.semsoftware.core.repository.RoomRepository;
import com.semsoftware.core.service.EntityConverter;
import com.semsoftware.core.util.Constants;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Query;
import java.util.List;

@Named
public class RoomRepositoryImpl extends GenericRepositoryImpl<RoomDTO> implements RoomRepository {

  @Inject
  private EntityConverter entityConverter;

  @Override
  public List<RoomDTO> findAllRoomDTOs() {
    Query query = em.createNamedQuery(Constants.QUERY_ALL_ROOMS);
    return query.getResultList();
  }

  

}