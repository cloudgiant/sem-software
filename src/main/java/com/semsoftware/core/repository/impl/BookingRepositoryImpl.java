package com.semsoftware.core.repository.impl;

import com.semsoftware.core.dto.BookingDTO;
import com.semsoftware.core.dto.ClientDTO;
import com.semsoftware.core.dto.RoomDTO;
import com.semsoftware.core.model.Booking;
import com.semsoftware.core.repository.BookingRepository;
import com.semsoftware.core.service.EntityConverter;
import com.semsoftware.core.util.Constants;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

@Named
public class BookingRepositoryImpl extends GenericRepositoryImpl<Booking> implements BookingRepository{

  @Inject
  private EntityConverter entityConverter;
  
  @Override
  public List<BookingDTO> findAllBookingDTOs() {
    Query query = em.createNamedQuery(Constants.QUERY_ALL_BOOKINGS);
    return query.getResultList();
  }

  @Override
  public List<ClientDTO> findAllClientsForBooking(Integer id) {
    Query query = em.createNamedQuery(Constants.QUERY_ALL_CLIENTS_FOR_BOOKING);
    List<ClientDTO> list = query.setParameter("id", id).getResultList();
    return list;
  }

  @Override
  public RoomDTO findRoomForBooking(Integer id) {
    Query query = em.createNamedQuery(Constants.QUERY_ROOM_FOR_BOOKING);
    List<RoomDTO> list = query.setParameter("id", id).getResultList();

    if (!list.isEmpty()) {
      return list.get(0);
    } else {
      return null;
    }
  }

  @Override
  public RoomDTO findBookedRoomForClient(ClientDTO client) {
    Query query = em.createNamedQuery(Constants.QUERY_BOOKED_ROOM_FOR_CLIENT);
    return null;
  }

  @Override
  public List<BookingDTO> findAllBookingsOnDate(Date date) {
    Query query = em.createNamedQuery(Constants.QUERY_ALL_BOOKINGS_ON_DATE);
    query.setParameter("dateFrom", date);
    return query.getResultList();
  }

  @Override
  public List<BookingDTO> findAllBookingsForClient(ClientDTO client) {
    Query query = em.createNamedQuery(Constants.QUERY_ALL_BOOKINGS_FOR_CLIENT);
    return null;
  }

  @Override
  public List<BookingDTO> findAllBookingsInPeriod(Date start, Date end) {
    Query query = em.createNamedQuery(Constants.QUERY_ALL_BOOKINGS_IN_PERIOD);
    query.setParameter("dateFrom", start);
    query.setParameter("dateTo", end);
    return query.getResultList();
  }

  @Override
  public List<BookingDTO> findAllBookingsWithDiscount() {
    Query query = em.createNamedQuery(Constants.QUERY_ALL_BOOKINGS_WITH_DISCOUNT);
    return query.getResultList();
  }

  @Override
  public BookingDTO findBookingOnId(Integer id) {
    Query query = em.createNamedQuery(Constants.QUERY_BOOKING_ON_ID);
    List<BookingDTO> list = query.setParameter("id", id).getResultList();

    if (!list.isEmpty()) {
      return list.get(0);
    } else {
      return null;
    }
  }

  @Override
  public List<BookingDTO> findCanceledBookings() {
    Query query = em.createNamedQuery(Constants.QUERY_CANCELED_BOOKINGS);
    return query.getResultList();
  }

  @Override
  public List<BookingDTO> findActiveBookings() {
    Query query = em.createNamedQuery(Constants.QUERY_ACTIVE_BOOKINGS);
    return query.getResultList();
  }

  @Override
  public List<BookingDTO> findBookingsBetweenDates(Date from, Date to) {
    Query query = em.createNamedQuery(Constants.QUERY_BOOKINGS_BETWEEN_DATE);
    query.setParameter("dateFrom", from);
    query.setParameter("dateTo", to);
    return query.getResultList();
  }


}
