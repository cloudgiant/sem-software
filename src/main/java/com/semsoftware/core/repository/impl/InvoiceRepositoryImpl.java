package com.semsoftware.core.repository.impl;

import javax.inject.Named;

import com.semsoftware.core.model.Invoice;
import com.semsoftware.core.repository.InvoiceRepository;

@Named
public class InvoiceRepositoryImpl extends GenericRepositoryImpl<Invoice> implements InvoiceRepository{

}
