package com.semsoftware.core.dto;

/**
 * Created by tatianarybnikova on 21/12/15.
 */
public enum DataType {

    BOOKING("Booking");

    private String label;

    private DataType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public String toString() {
        return label;
    }
}
