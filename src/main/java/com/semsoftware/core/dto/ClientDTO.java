package com.semsoftware.core.dto;

import com.semsoftware.core.model.Client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ClientDTO implements Serializable {

    private Integer id;
    private String firstName;
    private String lastName;
    private String email;
    private Date birthDate;
    private String gender;
    private String companyName;
    private String telephone;
    private String note;
    private String address;
    private List<BookingDTO> bookings = new ArrayList<>();

    public ClientDTO() {
    }

    public ClientDTO(final Client c) {
        this.id = c.getId();
        this.firstName = c.getFirstName();
        this.lastName = c.getLastName();
        this.email = c.getEmail();
        this.birthDate = c.getBirthDate();
        this.gender = c.getGender();
        this.companyName = c.getCompanyName();
        this.note = c.getNote();
        this.address = c.getAddress();
        this.telephone = c.getTelephone();

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }


    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public List<BookingDTO> getBookings() {
        return bookings;
    }


    public void setBookings(List<BookingDTO> bookings) {
        this.bookings = bookings;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
