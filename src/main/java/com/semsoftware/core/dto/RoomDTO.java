package com.semsoftware.core.dto;

import com.semsoftware.core.model.Room;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class RoomDTO implements Serializable{
  
  private Integer id;
  private String name;
  private Integer maxPeople;
  private Double priceSingle;
  private Double priceMax;
  private Double priceForChild;
    private List<BookingDTO> bookings = new ArrayList<BookingDTO>();
    private List<InvoiceDTO> invoices = new ArrayList<InvoiceDTO>();
    private String facilities;
    private Integer size;


  public RoomDTO() {
  }

  public RoomDTO(final Room r) {
    this.id=r.getId();
    this.name=r.getName();
    this.maxPeople=r.getMaxPeople();
    this.priceMax=r.getPriceMax();
    this.priceForChild=r.getPriceForChild();
    this.priceSingle=r.getPriceSingle();
    this.size = r.getSize();
    this.facilities = r.getFacilities();
  }
  
  public Integer getId() {
      return id;
  }

  public void setId(Integer id) {
      this.id = id;
  }

  public String getName() {
      return name;
  }

  public void setName(String name) {
      this.name = name;
  }

  public Integer getMaxPeople() {
      return maxPeople;
  }

  public void setMaxPeople(Integer maxPeople) {
      this.maxPeople = maxPeople;
  }

  public Double getPriceSingle() {
      return priceSingle;
  }

  public void setPriceSingle(Double priceSingle) {
      this.priceSingle = priceSingle;
  }

    public Double getPriceMax() {
        return priceMax;
    }

    public void setPriceMax(Double priceMax) {
        this.priceMax = priceMax;
    }

    public Double getPriceForChild() {
        return priceForChild;
    }

    public void setPriceForChild(Double priceForChild) {
        this.priceForChild = priceForChild;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public List<BookingDTO> getBookings() {
        return bookings;
  }


    public void setBookings(List<BookingDTO> bookings) {
        this.bookings = bookings;
  }

    public List<InvoiceDTO> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<InvoiceDTO> invoices) {
        this.invoices = invoices;
    }

    public String getFacilities() {
        return facilities;
    }

    public void setFacilities(String facilities) {
        this.facilities = facilities;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
