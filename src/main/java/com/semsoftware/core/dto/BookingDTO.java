package com.semsoftware.core.dto;

import com.semsoftware.core.model.Booking;
import com.semsoftware.core.model.Client;
import com.semsoftware.core.model.Room;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BookingDTO implements Serializable{
  
  private Integer id;
  private List<ClientDTO> clients = new ArrayList<ClientDTO>();
  private List<RoomDTO> rooms = new ArrayList<RoomDTO>();
  private Double price;
  private Integer duration;
  private Date dateFrom;
  private Date dateTo;
  private Boolean cancel;


  public BookingDTO() {
  }
  
  public BookingDTO(final Booking b) {
    this.id=b.getId();
    List<Client> l = b.getClients();
    if(l!=null){
      for(Client c: l){
        this.clients.add(new ClientDTO(c));
      }
    }
    List<Room> rl = b.getRooms();
    if (rl != null) {
      for (Room r : rl) {
        this.rooms.add(new RoomDTO(r));
      }
    }
    this.price=b.getPrice();
    this.duration=b.getDuration();
    this.dateFrom=b.getDateFrom();
    this.dateTo=b.getDateTo();
    this.cancel=b.getCancel();
    
  }


  public Integer getId() {
      return id;
  }


  public List<ClientDTO> getClients() {
      return clients;
  }

  public void setClients(List<ClientDTO> clients) {
      this.clients = clients;
  }

  public List<RoomDTO> getRooms() {
    return rooms;
  }

  public void setRooms(List<RoomDTO> rooms) {
    this.rooms = rooms;
  }

  public Double getPrice() {
      return price;
  }

  public void setPrice(Double price) {
      this.price = price;
  }

  public Integer getDuration() {
      return duration;
  }

  public void setDuration(Integer duration) {
      this.duration = duration;
  }


  public Date getDateFrom() {
    return dateFrom;
  }


  public void setDateFrom(Date dateFrom) {
    this.dateFrom = dateFrom;
  }


  public Date getDateTo() {
    return dateTo;
  }


  public void setDateTo(Date dateTo) {
    this.dateTo = dateTo;
  }

  public Boolean getCancel() {
    return cancel;
  }

  public void setCancel(Boolean cancel) {
    this.cancel = cancel;
  }
}
