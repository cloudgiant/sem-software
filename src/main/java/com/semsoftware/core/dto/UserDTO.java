package com.semsoftware.core.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import com.semsoftware.core.Role;
import com.semsoftware.core.model.User;

public class UserDTO implements Serializable {

    private Integer id;
    private String firstName;
    private String lastName;
    private String userName;
    private byte[] password;
    private byte[] salt;
    private String email;
    private Date birthDate;
    private boolean deleted;
    private boolean online;
    private Role role;
    private String password_clear;


    public UserDTO() {
    }

    public UserDTO(final Integer id, final String firstName, final String lastName,
                   final String userName, final String email, Date birthDate, Role role) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.email = email;
        this.birthDate = birthDate;
        this.role = role;

    }

    public UserDTO(final User u) {
        this.id = u.getId();
        this.firstName = u.getFirstName();
        this.lastName = u.getLastName();
        this.password = u.getPassword();
        this.userName = u.getUserName();
        this.salt = u.getSalt();
        this.email = u.getEmail();
        this.role = u.getRole();
        this.birthDate = u.getBirthDate();

        this.setOnline(true);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public byte[] getPassword() {
        return password;
    }

    public void setPassword(byte[] password) {
        this.password = password;
    }

    public byte[] getSalt() {
        return salt;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getPassword_clear() {
        return password_clear;
    }

    public void setPassword_clear(String password_clear) {
        this.password_clear = password_clear;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

}
