package com.semsoftware.core.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.semsoftware.core.model.Client;
import com.semsoftware.core.model.Invoice;
import com.semsoftware.core.model.Room;


public class InvoiceDTO implements Serializable{
  
  private Integer id;
  private List<ClientDTO> clients = new ArrayList<>();
  private List<RoomDTO> rooms = new ArrayList<>();
  private String text;
  private Double price;
  private String addressHotel;
  private String addressClient;
  private Integer duration;
  private Date createdOn;

  public InvoiceDTO() {
  }
  
  public InvoiceDTO(final Invoice i) {
    this.id=i.getId();
    List<Client> l1= i.getClients();
    for(Client c:l1){
      this.clients.add(new ClientDTO(c));
    }
    List<Room> l2= i.getRooms();
    for(Room r:l2){
      this.rooms.add(new RoomDTO(r));
    }
    this.text=i.getText();
    this.price=i.getPrice();
    this.addressClient=i.getAddressClient();
    this.addressHotel=i.getAddressHotel();
    this.duration=i.getDuration();
    this.createdOn=i.getCreatedOn();
    
  }


  public Integer getId() {
      return id;
  }


  public List<ClientDTO> getClients() {
      return clients;
  }

  public void setClients(List<ClientDTO> clients) {
      this.clients = clients;
  }

  public List<RoomDTO> getRooms() {
      return rooms;
  }

  public void setRooms(List<RoomDTO> rooms) {
      this.rooms = rooms;
  }

  public String getText() {
      return text;
  }

  public void setText(String text) {
      this.text = text;
  }

  public Double getPrice() {
      return price;
  }

  public void setPrice(Double price) {
      this.price = price;
  }

  public String getAddressHotel() {
      return addressHotel;
  }

  public void setAddressHotel(String addressHotel) {
      this.addressHotel = addressHotel;
  }

  public String getAddressClient() {
      return addressClient;
  }

  public void setAddressClient(String addressClient) {
      this.addressClient = addressClient;
  }

  public Integer getDuration() {
      return duration;
  }

  public void setDuration(Integer duration) {
      this.duration = duration;
  }

  public Date getCreatedOn() {
      return createdOn;
  }

  public void setCreatedOn(Date createdOn) {
      this.createdOn = createdOn;
  }
}
