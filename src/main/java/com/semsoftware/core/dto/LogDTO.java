package com.semsoftware.core.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Martina on 06.01.2016.
 */
public class LogDTO implements Serializable {

    private String userId;
    private Date date;
    private String logger;
    private String level;
    private String message;

    public LogDTO() {

    }

    public LogDTO(String userId, Date date, String logger, String level, String message) {
        this.userId = userId;
        this.date = date;
        this.logger = logger;
        this.level = level;
        this.message = message;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLogger() {
        return logger;
    }

    public void setLogger(String logger) {
        this.logger = logger;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
