package com.semsoftware.core.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Filter Class for Checking Authentication of the User
 * 
 * @author ozdesimsek
 *
 */
public class AuthenticationFilter implements Filter{

  private static final Logger logger = LoggerFactory.getLogger(AuthenticationFilter.class);
  
  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    try {
      // check whether session variable is set
      HttpServletRequest req = (HttpServletRequest) request;
      HttpServletResponse res = (HttpServletResponse) response;
      HttpSession ses = req.getSession(false);
      String userAgent = req.getHeader("user-agent");
      String accept = req.getHeader("Accept");

      if (userAgent != null && accept != null) {
        String reqURI = req.getRequestURI();       

        // Now check if logged in
        if (reqURI.indexOf("/public/index.jsf") >= 0
            && (ses != null && ses.getAttribute("username") != null)) {
          res.sendRedirect(req.getContextPath() + "/pages/main.jsf");
          return;
        }
        if (reqURI.indexOf("/public/") >= 0
            || (ses != null && ses.getAttribute("username") != null)
            || reqURI.contains("javax.faces.resource")) {
          chain.doFilter(request, response);
        } else {
          logger.info("Not logged in");
          String path = "/public/index.jsf";
          res.sendRedirect(req.getContextPath() + path);
        }
      }
    } catch (Exception t) {
      logger.error(t.getMessage());
    }
    
  }

  @Override
  public void destroy() {
    // TODO Auto-generated method stub
    
  }

}
