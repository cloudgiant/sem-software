package com.semsoftware.core.util;

/**
 * 
 * @author ozdesimsek
 *
 */
public class Constants {
  
  /*
   * 
   * Resource Bundles
   */
  public static final String I18N_MESSAGE_BUNDLE_PATH = "com/semsoftware/i18n/messages";;
  public static final String I18N_VALIDATION_BUNDLE_PATH = "com/semsoftware/i18n/validationMessages";
  public static final String PAGE_REDIRECT = "?faces-redirect=true";
  public static final String FIND_ALL_USERS = "findAllUsers";
  public static final String QUERY_FIND_USER_BY_EMAIL = "findUsersByMail";
  public static final String QUERY_FIND_USER_NAME_EMAIL = "findUserByNameEmail";
  public static final String QUERY_ALL_ADMINS = "findAllAdmins";
  public static final String QUERY_ALL_BOOKINGS = "findAllBookings";
  public static final String QUERY_ALL_ROOMS = "findAllRooms";
  public static final String QUERY_ALL_CLIENTS = "findAllClients";
  public static final String QUERY_ALL_CLIENTS_FOR_BOOKING = "findAllClientsForBooking";
  public static final String QUERY_ROOM_FOR_BOOKING = "findRoomForBooking";
  public static final String QUERY_BOOKED_ROOM_FOR_CLIENT = "findBookedRoomForClient";
  public static final String QUERY_ALL_BOOKINGS_ON_DATE = "findAllBookingsOnDate";
  public static final String QUERY_ALL_BOOKINGS_IN_PERIOD = "findAllBookingsInPeriod";
  public static final String QUERY_ALL_BOOKINGS_FOR_CLIENT = "findAllBookingsForClient";
  public static final String QUERY_ALL_BOOKINGS_WITH_DISCOUNT = "findAllBookingsWithDiscount";
  public static final String QUERY_BOOKING_ON_ID = "findBookingOnId";
  public static final String QUERY_CANCELED_BOOKINGS ="findCanceledBookings";
  public static final String QUERY_ACTIVE_BOOKINGS ="findActiveBookings";
  public static final String QUERY_BOOKINGS_BETWEEN_DATE ="findBookingsBetweenDate";

}
