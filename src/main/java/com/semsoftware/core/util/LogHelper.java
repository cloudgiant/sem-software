package com.semsoftware.core.util;

import javax.servlet.http.HttpSession;

/**
 * Created by Tatiana on 10/01/16.
 */
public class LogHelper {

    public static final String USERACTION = "USERACTION: ";

    public static String getMsgHeadUsr(){
        HttpSession session = HttpUtil.getSession();
        return USERACTION + session.getAttribute("username").toString() + " - ";
    }

    public static String getMsgHead(){
        return USERACTION;
    }
}
