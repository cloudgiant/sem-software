package com.semsoftware.core.controllers;


import com.semsoftware.core.controllers.user.App;
import com.semsoftware.core.dto.BookingDTO;
import com.semsoftware.core.dto.ClientDTO;
import com.semsoftware.core.dto.RoomDTO;
import com.semsoftware.core.facade.BookingFacade;
import com.semsoftware.core.facade.ClientFacade;
import com.semsoftware.core.facade.RoomFacade;
import com.semsoftware.core.util.PrimeFacesUtil;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.SelectEvent;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.*;

@Named("bookingBean")
@Scope("view")
public class BookingBean implements Serializable{

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Inject
  private NavigationBean navigation;
  @Inject
  private App app;
  @Inject
  private BookingFacade bookingFacade;
  @Inject
  private ClientFacade clientFacade;
  @Inject
  private RoomFacade roomFacade;
  
  private List<BookingDTO> bookings;
  private List<ClientDTO> clients;
  private List<ClientDTO> selectedClients = new ArrayList<ClientDTO>();
  private String[] selectedClientsStr;   
  private List<String> clientsStr;
  private List<RoomDTO> rooms;
  private RoomDTO selectedRoom;
  private List<RoomDTO> selectedRooms=new ArrayList<RoomDTO>();
  private String selectedRoomsStr="";   
  private List<String> roomsStr;
  private double price=0.0, total=0.0;
  private Integer duration;
  private Date dateFrom;
  private Date dateTo;
  private String roomList;
  private HashMap<RoomDTO, Double> roomMap = new HashMap<RoomDTO, Double>();
  private List<String> roomAndPreis =  new ArrayList<String>();
  private Integer erwachseneNr=1, kinderNr=0;
  private ArrayList<Integer> selectedRoomIds = new ArrayList<Integer>();
  private Date today;
  private Date dateFromPlusOne;
  private String returnPage;
  private BookingDTO booking;  
  
  @PostConstruct
  public void init() {
    
    final Map<String, String> requestParameterMap = PrimeFacesUtil.getRequestParameterMap();
    if (requestParameterMap != null) {
      String bookingId = requestParameterMap.get("bookingId");
      returnPage = requestParameterMap.get("returnPage");
      
      if(bookingId!=null){
        booking = bookingFacade.getBookingOnId(Integer.parseInt(bookingId));
        dateFrom = booking.getDateFrom();
        dateTo = booking.getDateTo();
        duration = booking.getDuration();
        total = booking.getPrice();
        selectedClients = booking.getClients();
        selectedClientsStr = new String[selectedClients.size()];
        int i=0;
        for(ClientDTO c: selectedClients){
          selectedClientsStr[i] = (c.getId()+" "+c.getFirstName()+" "+c.getLastName());
          i++;
        }
      }else{
        booking = new BookingDTO();
      }
      
    }
    
    bookings = bookingFacade.findActiveBookings();
    clients = clientFacade.getAllClientDTOs();
    makeClientList();

    rooms = roomFacade.findAll();    
    
    
  }

  public BookingDTO getBooking() {
    return booking;
  }

  public void setBooking(BookingDTO booking) {
    this.booking = booking;
  }

  public void makeClientList(){
    
    clientsStr =  new ArrayList<String>();
    for(ClientDTO c: clients){
      clientsStr.add(c.getId()+" "+c.getFirstName()+" "+c.getLastName());
    }
    
  }

  public String createBooking() {
//    BookingDTO bookingDTO = new BookingDTO();
    
    for(ClientDTO c: selectedClients){
      System.out.println(c.getId());
    }
    booking.setClients(selectedClients);
    booking.setRooms(selectedRooms);
    booking.setDateFrom(dateFrom);
    booking.setDateTo(dateTo);
    booking.setPrice(getTotal());
    
    booking.setCancel(false);

    booking.setDuration(duration);

    bookingFacade.saveBooking(booking);

    return navigation.getPageBookingList();
  }
  
  public void refreshZimmerListe(){
    int max = erwachseneNr;
    roomsStr =  new ArrayList<String>();
    System.out.println(max);
    boolean addRoom=true;
    List<BookingDTO> bookingsBetweenDates = bookingFacade.findBookingsBetweenDates(dateFrom, dateTo);
    System.out.println("bookingsBetweenDates size: "+bookingsBetweenDates.size());
    for(RoomDTO r: rooms){
        if(r.getMaxPeople()>= max && !selectedRoomIds.contains(r.getId())){
          for(BookingDTO b: bookingsBetweenDates){
            List<RoomDTO> listroom= b.getRooms();
            for(RoomDTO r2: listroom){
  //            System.out.println(r2.getId()+" "+r2.getName());
              if(r2.getId()==r.getId()){ 
                  
                    addRoom=false;
                  
              
              }
              
            }
          }
          
          if(addRoom==true){
            roomsStr.add(r.getId()+" "+r.getName());
              if (selectedRoomsStr == null) {
                  selectedRoomsStr = r.getId()+" "+r.getName();
              }
          }
        }
        addRoom=true;
    }

    
  }
  
  public void addZimmer(){
      if (selectedRoomsStr != null) {
          String[] str = selectedRoomsStr.split("\\s+");
          try {
              int id = Integer.parseInt(str[0]);

              for (RoomDTO r : rooms) {
                  if (id == r.getId()) {
                      selectedRoom = r;
                  }
              }
              double preis = 0;
              if (erwachseneNr == 1) {
                  preis += selectedRoom.getPriceSingle();
              } else {
                  preis += selectedRoom.getPriceMax();
              }
              if (kinderNr != 0) {
                  preis += (selectedRoom.getPriceForChild()) * kinderNr;
              }
              roomMap.put(selectedRoom, preis);
              selectedRooms.add(selectedRoom);
              setPrice(preis);
              selectedRoomIds.add(selectedRoom.getId());
              roomAndPreis.add(selectedRoom.getId() + " " + selectedRoom.getName() + " - Preis: " + preis);
              //refreshZimmerListe();
          } catch (Exception e) {
              e.printStackTrace();
          }
      }
  }
  
  public List<BookingDTO> getBookings() {
    return bookings;
  }

  public void setBookings(List<BookingDTO> bookings) {
    this.bookings = bookings;
  }

  public List<ClientDTO> getClients() {
    return clients;
  }

  public void setClients(List<ClientDTO> clients) {
    this.clients = clients;
  }

  public List<ClientDTO> getSelectedClients() {
    return selectedClients;
  }

  public void setSelectedClients(List<ClientDTO> selectedClients) {
    this.selectedClients = selectedClients;
  }

  public String[] getSelectedClientsStr() {
    return selectedClientsStr;
  }

  public void setSelectedClientsStr(String[] selectedClientsStr) {
    this.selectedClientsStr = selectedClientsStr;
  }

  public List<String> getClientsStr() {
    return clientsStr;
  }

  public void setClientsStr(List<String> clientsStr) {
    this.clientsStr = clientsStr;
  }

  public List<RoomDTO> getRooms() {
    return rooms;
  }

  public void setRooms(List<RoomDTO> rooms) {
    this.rooms = rooms;
  }

  public List<RoomDTO> getSelectedRooms() {
    return selectedRooms;
  }

  public void setSelectedRooms(List<RoomDTO> selectedRooms) {
    this.selectedRooms = selectedRooms;
  }

  public String getSelectedRoomsStr() {
    return selectedRoomsStr;
  }

  public void setSelectedRoomsStr(String selectedRoomsStr) {
    this.selectedRoomsStr = selectedRoomsStr;
  }

  public List<String> getRoomsStr() {
    return roomsStr;
  }

  public void setRoomsStr(List<String> roomsStr) {
    this.roomsStr = roomsStr;
  }

  public double getPrice() {

    double p=0;
    for (Map.Entry<RoomDTO, Double> entry : roomMap.entrySet()) {
       p+=entry.getValue();
    }
    
    setPrice(p);
    return price;
  }

  public void setPrice(double price) {
    this.price = price;

  }

  public Integer getDuration() {
    return duration;
  }

  public void setDuration(Integer duration) {
    this.duration = duration;
  }

  public Date getDateFrom() {
    return dateFrom;
  }

  public void setDateFrom(Date dateFrom) {
    this.dateFrom = dateFrom;
  }

  public Date getDateTo() {
    return dateTo;
  }

  public void setDateTo(Date dateTo) {
    this.dateTo = dateTo;
  }

  public String getRoomList() {
    return roomList;
  }

  public void setRoomList(String roomList) {
    this.roomList = roomList;
  }

  public HashMap<RoomDTO, Double> getRoomMap() {
    return roomMap;
  }

  public void setRoomMap(HashMap<RoomDTO, Double> roomMap) {
    this.roomMap = roomMap;
  }

  public RoomDTO getSelectedRoom() {
    return selectedRoom;
  }

  public void setSelectedRoom(RoomDTO selectedRoom) {
    this.selectedRoom = selectedRoom;
  }

  public Integer getKinderNr() {
    return kinderNr;
  }

  public void setKinderNr(Integer kinderNr) {
    this.kinderNr = kinderNr;
  }

  public Integer getErwachseneNr() {
    return erwachseneNr;
  }

  public void setErwachseneNr(Integer erwachseneNr) {
    this.erwachseneNr = erwachseneNr;
  }
  
  public String onFlowProcess(FlowEvent event) {
    
    if(event.getNewStep().equals("preis")){
      if(roomAndPreis.size()>0){
        return event.getNewStep();
      }else {
        return "zimmer";
      }
    }
    if(event.getNewStep().equals("zimmer")){
      duration = (int) ((getDateTo().getTime() - getDateFrom().getTime()) / (1000 * 60 * 60 * 24));
    }
    if(event.getNewStep().equals("datum")){
      selectedClients= new ArrayList<ClientDTO>();
      for (int i = 0; i < getSelectedClientsStr().length; i++) {
        String[] splitted = getSelectedClientsStr()[i].split("\\s+");
        for (ClientDTO c : clients) {
          if (c.getId().toString().equals(splitted[0]) && !selectedClients.contains(c)) {
            selectedClients.add(c);
          }
        }
      }
    }
    return event.getNewStep();
    
  }

  public List<String> getRoomAndPreis() {
    return roomAndPreis;
  }

  public void setRoomAndPreis(List<String> roomAndPreis) {
    this.roomAndPreis = roomAndPreis;
  }

  public double getTotal() {
    total=getPrice()*getDuration();
    return total;
  }

  public void setTotal(double total) {
    this.total = total;
  }

  public Date getToday() {
    Calendar c = Calendar.getInstance(); 
    return c.getTime();
  }

  public void setToday(Date today) {
    this.today = today;
  }

  public Date getDateFromPlusOne() {
    
    Calendar c = Calendar.getInstance(); 
    if(dateFrom!=null){
      c.setTime(dateFrom);
    }
    c.add(Calendar.DATE, 1);
    System.out.println(c.getTime().toGMTString());
    return c.getTime();
  }

  public void setDateFromPlusOne(Date dateFromPlusOne) {
    this.dateFromPlusOne = dateFromPlusOne;
  }
  
  public void updateDateTo(AjaxBehaviorEvent event){
    SelectEvent slEv = (SelectEvent) event;
    dateFrom = (Date) slEv.getObject();
    getDateFromPlusOne();
  }

  
}
