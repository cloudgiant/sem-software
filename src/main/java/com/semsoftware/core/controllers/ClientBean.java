package com.semsoftware.core.controllers;


import com.semsoftware.core.controllers.user.App;
import com.semsoftware.core.dto.ClientDTO;
import com.semsoftware.core.facade.ClientFacade;
import com.semsoftware.core.service.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Named("clientBean")
@Scope("view")
public class ClientBean implements Serializable{
  /**
   *
   */
  private static final long serialVersionUID = 1L;
  private static final Logger LOGGER = LoggerFactory.getLogger(ClientBean.class);
  @Inject
  ClientFacade facade;
  @Inject
  ClientService clientService;
  @Inject
  private NavigationBean navigation;
  @Inject
  private App app;

  private List<ClientDTO> clients;

  private Integer id;
  private String firstName;
  private String lastName;
  private String email;
  private Date birthDate;
  private String gender;
  private String companyName;
  //private Double discount;
  private String telephone;
  //  private String web;
//  private String fax;
  private String note;
  private String address;

  @PostConstruct
  public void init() {
    LOGGER.info("Bean created");

    if (clients == null) {
      clients = facade.getAllClientDTOs();
    }
  }

  public List<ClientDTO> getClients() {
    return clients;
  }


  public String createClient(){
    ClientDTO clientDTO = new ClientDTO();
    clientDTO.setId(id);
    System.out.println("lololol " + firstName);
    clientDTO.setFirstName(firstName);
    clientDTO.setLastName(lastName);
    clientDTO.setEmail(email);
    clientDTO.setBirthDate(birthDate);
    clientDTO.setGender(gender);
    clientDTO.setCompanyName(companyName);
    clientDTO.setTelephone(telephone);
    clientDTO.setAddress(address);
    clientDTO.setNote(note);
    clientService.createClient(clientDTO);
    refreshClients();
    return navigation.getPageClientList();
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Date getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(Date birthDate) {
    this.birthDate = birthDate;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getTelephone() {
    return telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  private void refreshClients(){
    clients = facade.getAllClientDTOs();
  }
}
