package com.semsoftware.core.controllers;

import com.semsoftware.core.dto.BookingDTO;
import com.semsoftware.core.dto.ClientDTO;
import com.semsoftware.core.dto.RoomDTO;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.context.annotation.Scope;

import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Martina on 08.01.2016.
 */
@ManagedBean
@Named("invoicePdf")
@Scope("session")
public class InvoiceDownloadBean {

    private StreamedContent file;


    public StreamedContent createPdf(final BookingDTO bookingDTO) {

        PDDocument document = createPdfContent(bookingDTO);
        String pathToDocument = null;
        try {
            document.save("rechnung.pdf");
            pathToDocument = new File("rechnung.pdf").getAbsolutePath();
            document.close();
            InputStream stream = new FileInputStream(pathToDocument);
            file = new DefaultStreamedContent(stream, "application/pdf", "rechnung.pdf");


        } catch (IOException e) {
            e.printStackTrace();
        }


        return file;
    }


    public PDDocument createPdfContent(BookingDTO bookingDTO) {

        PDDocument document = null;

        try {
            // Create a document and add a page to it

            document = new PDDocument();
            PDPage page = new PDPage();
            document.addPage(page);

            // Start a new content stream which will "hold" the to be created content

            PDPageContentStream contentStream = new PDPageContentStream(document, page);

            // Define a text content stream using the selected font, moving the cursor and drawing the text

            float fontSize = 12;
            float leading = 1.5f * fontSize;
            PDRectangle mediabox = page.getMediaBox();
            float margin = 72;
            float width = mediabox.getWidth() - 2 * margin;
            float startX = mediabox.getLowerLeftX() + margin;
            float startY = mediabox.getUpperRightY() - margin;
            int lastSpace = -1;
            float heightCounter = startY;

            contentStream.beginText();

            // Create a new font object selecting one of the PDF base fonts
            contentStream.setFont(PDType1Font.HELVETICA_BOLD, 16);
            contentStream.newLineAtOffset(startX, startY);

            contentStream.showText("Rechnung");
            contentStream.newLineAtOffset(0, -leading);
            contentStream.newLineAtOffset(0, -leading);
            heightCounter = heightCounter - (margin + fontSize);


            contentStream.setFont(PDType1Font.HELVETICA, 12);

            DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            String date = formatter.format(new Date());

            contentStream.showText("Datum   " + date);
            contentStream.newLineAtOffset(0, -leading);
            heightCounter = heightCounter - (margin + fontSize);
            contentStream.newLineAtOffset(0, -leading);

            contentStream.showText("Bunchungsnummer:   ");
            contentStream.showText(String.valueOf(bookingDTO.getId()));
            contentStream.newLineAtOffset(0, -leading);
            heightCounter = heightCounter - (margin + fontSize);
            contentStream.newLineAtOffset(0, -leading);

            contentStream.setFont(PDType1Font.HELVETICA_BOLD, 12);
            contentStream.showText("Kunden:");
            contentStream.newLineAtOffset(0, -leading);
            heightCounter = heightCounter - (margin + fontSize);

            contentStream.setFont(PDType1Font.HELVETICA, 12);

            for (ClientDTO c : bookingDTO.getClients()) {

                contentStream.showText(String.valueOf(c.getId()) + "    ");
                contentStream.showText(c.getFirstName() + " " + c.getLastName() + "   ");
                contentStream.showText(c.getAddress());
                contentStream.newLineAtOffset(0, -leading);
                heightCounter = heightCounter - (margin + fontSize);

            }
            contentStream.newLineAtOffset(0, -leading);
            heightCounter = heightCounter - (margin + fontSize);

            contentStream.setFont(PDType1Font.HELVETICA_BOLD, 12);
            contentStream.showText("Datum:  ");
            contentStream.setFont(PDType1Font.HELVETICA, 12);
            contentStream.showText("Anreise  " + bookingDTO.getDateFrom().toString() + "  Abreise  " + bookingDTO.getDateTo().toString());
            contentStream.newLineAtOffset(0, -leading);
            heightCounter = heightCounter - (margin + fontSize);
            contentStream.newLineAtOffset(0, -leading);

            contentStream.setFont(PDType1Font.HELVETICA_BOLD, 12);
            contentStream.showText("Zimmer:  ");
            contentStream.newLineAtOffset(0, -leading);
            heightCounter = heightCounter - (margin + fontSize);
            contentStream.setFont(PDType1Font.HELVETICA, 12);

            for (RoomDTO r : bookingDTO.getRooms()) {

                contentStream.showText(String.valueOf(r.getId()) + "   ");
                contentStream.showText(r.getName() + "   ");
                contentStream.newLineAtOffset(0, -leading);
                heightCounter = heightCounter - (margin + fontSize);
            }
            contentStream.newLineAtOffset(0, -leading);
            heightCounter = heightCounter - (margin + fontSize);

            contentStream.setFont(PDType1Font.HELVETICA_BOLD, 12);
            contentStream.showText("Gesamtpreis:  " + String.valueOf(bookingDTO.getPrice()));
            contentStream.newLineAtOffset(0, -leading);
            heightCounter = heightCounter - (margin + fontSize);
            contentStream.newLineAtOffset(0, -leading);

            contentStream.setFont(PDType1Font.HELVETICA, 12);
            contentStream.showText("Eine kostenlose Stornierung ist bis [2 Tage vor Anreise möglich] möglich.");
            contentStream.newLineAtOffset(0, -leading);
            heightCounter = heightCounter - (margin + fontSize);
            contentStream.newLineAtOffset(0, -leading);

            contentStream.setFont(PDType1Font.HELVETICA, 12);
            contentStream.showText("Wir wuenschen Ihnen schon heute eine angenehme Anreise und");
            contentStream.newLineAtOffset(0, -leading);
            heightCounter = heightCounter - (margin + fontSize);


            contentStream.showText("einen schoe Aufenthalt in unserem Haus!");

            contentStream.newLineAtOffset(0, -leading);
            heightCounter = heightCounter - (margin + fontSize);
            contentStream.newLineAtOffset(0, -leading);

            contentStream.showText("Mit freundlichen Grueßen");

            contentStream.newLineAtOffset(0, -leading);
            heightCounter = heightCounter - (margin + fontSize);
            contentStream.newLineAtOffset(0, -leading);
            contentStream.showText("Hotel Zum Goldenen Loewen");

            contentStream.newLineAtOffset(0, -leading);
            heightCounter = heightCounter - (margin + fontSize);

            contentStream.showText("Warschauer Strasse 57");

            contentStream.newLineAtOffset(0, -leading);
            heightCounter = heightCounter - (margin + fontSize);

            contentStream.showText("1010 Wien");

            contentStream.newLineAtOffset(0, -leading);
            heightCounter = heightCounter - (margin + fontSize);
            contentStream.newLineAtOffset(0, -leading);

            contentStream.showText("030-97 00 20 30");

            contentStream.newLineAtOffset(0, -leading);
            heightCounter = heightCounter - (margin + fontSize);

            contentStream.showText("030-97 00 20 31");
            contentStream.newLineAtOffset(0, -leading);

            contentStream.endText();

            // Make sure that the content stream is closed
            contentStream.close();


        } catch (IOException e) {
            e.printStackTrace();
        }

        return document;
    }
}
