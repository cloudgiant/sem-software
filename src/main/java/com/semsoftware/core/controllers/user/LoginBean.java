package com.semsoftware.core.controllers.user;

import com.semsoftware.core.controllers.NavigationBean;
import com.semsoftware.core.dto.UserDTO;
import com.semsoftware.core.facade.UserFacade;
import com.semsoftware.core.util.HttpUtil;
import com.semsoftware.core.util.LogHelper;
import com.semsoftware.core.util.PrimeFacesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * 
 * @author ozdesimsek
 *
 */
@Named("login")
@Scope("view")
public class LoginBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginBean.class);
    private ResourceBundle valMs = App.getValidationBundle();
    @Inject
    private UserFacade userFacade;
    @Inject
    private NavigationBean navigation;
    @Inject
    private UserBean userBean;
    @Inject
    private App app;
    private String password;
    private String emailUsername;

    @PostConstruct
    public void init() {
      final Map<String, String> requestParameterMap = PrimeFacesUtil.getRequestParameterMap();
      final String userId = requestParameterMap.get("userId");
      final String token = requestParameterMap.get("token");
      final String msg = requestParameterMap.get("msg");
      final String uname = requestParameterMap.get("uname");
      System.out.println("MSG IS : " + msg);
    }

    public String login() {
      UserDTO currentUser = userFacade.login(emailUsername, password);
      if (currentUser == null) {
        PrimeFacesUtil.addValidationMessage(FacesMessage.SEVERITY_WARN,
            valMs.getString("error.credentials.invalid"));
        return navigation.getPageIndex();
      } else {
        HttpSession session = HttpUtil.getSession();
        session.setAttribute("username", emailUsername);
        LOGGER.info(LogHelper.getMsgHeadUsr() + "LOGGED IN");
        userBean.setCurrentUser(currentUser);                
        app.addOnlineUser(currentUser);
        

        return navigation.getPageHome();
      }
    }

    public String getPassword() {
      return password;
    }

    public void setPassword(String password) {
      this.password = password;
    }

    public String getEmailUsername() {
      return emailUsername;
    }

    public void setEmailUsername(String emailUsername) {
      this.emailUsername = emailUsername;
    }

}
