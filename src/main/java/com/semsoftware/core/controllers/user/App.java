package com.semsoftware.core.controllers.user;

import com.semsoftware.core.dto.UserDTO;
import com.semsoftware.core.util.Constants;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.Serializable;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * 
 * @author ozdesimsek
 *
 */
@Named
@Singleton
public class App implements Serializable {


  private static ResourceBundle MESSAGE_BUNDLE = ResourceBundle
      .getBundle(Constants.I18N_MESSAGE_BUNDLE_PATH);
  private static ResourceBundle VALIDATION_MESSAGE_BUNDLE = ResourceBundle
      .getBundle(Constants.I18N_VALIDATION_BUNDLE_PATH);
  private String username;
  private Set<UserDTO> onlineUsers;

  public App() {}

  public static ResourceBundle getMessageBundle() {
    return MESSAGE_BUNDLE;
  }

  public static ResourceBundle getValidationBundle() {
    return VALIDATION_MESSAGE_BUNDLE;
  }

  @PostConstruct
  private void init() {
    if (onlineUsers == null) {
      this.onlineUsers = new HashSet<UserDTO>();
    }
  }

  public Set<UserDTO> getOnlineUsers() {
    return onlineUsers;
  }

  public void setOnlineUsers(Set<UserDTO> onlineUsers) {
    this.onlineUsers = onlineUsers;
  }

  public boolean isOnline(UserDTO user) {
    return onlineUsers.contains(user);
  }

  public void addOnlineUser(UserDTO user) {
    this.onlineUsers.add(user);
  }

  public boolean removeOnlineUser(UserDTO user) {
    return onlineUsers.remove(user);
  }




}

