package com.semsoftware.core.controllers.user;

import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import com.semsoftware.core.util.LogHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;

import com.semsoftware.core.controllers.NavigationBean;
import com.semsoftware.core.dto.UserDTO;
import com.semsoftware.core.facade.UserFacade;
import com.semsoftware.core.util.Constants;
import com.semsoftware.core.util.HttpUtil;
import com.semsoftware.core.util.PrimeFacesUtil;

/**
 * 
 * @author ozdesimsek
 *
 */
@Named("registrationBean")
@Scope("view")
public class RegisterBean {

  private static final Logger LOGGER = LoggerFactory.getLogger(RegisterBean.class);

  private ResourceBundle ms = App.getMessageBundle();

  private ResourceBundle valMs = App.getValidationBundle();

  @Inject
  private UserFacade userFacade;
  @Inject
  private NavigationBean navigation;
  private UserDTO currentUser;
  private String income;
  private String passwordMatch;
  private boolean gtc;

  @PostConstruct
  public void init() {
    if (currentUser == null) {
      currentUser = new UserDTO();
    }
  }

  public UserDTO getCurrentUser() {
    return currentUser;
  }


  public void setCurrentUser(UserDTO currentUser) {
    this.currentUser = currentUser;
  }

  public String createUser() {
    if (!currentUser.getPassword_clear().equals(passwordMatch)) {
      PrimeFacesUtil.addValidationMessage(FacesMessage.SEVERITY_ERROR,
          valMs.getString("error.password.match"));
      return null;
    }
    if (!userFacade.verifyUnique(currentUser.getUserName(), currentUser.getEmail())) {
      PrimeFacesUtil.addValidationMessage(FacesMessage.SEVERITY_ERROR,
          valMs.getString("error.nameemail.unqiue"));
      return null;
    }

    final String userToken = UUID.randomUUID().toString();

    currentUser = userFacade.createUser(currentUser);

    String baseURL = HttpUtil.getBaseURL();
    String pageLink =
        baseURL + navigation.getPageIndex() + ".jsf" + "?userId=" + currentUser.getId() + "&token="
            + userToken;

    return navigation.getPageIndex() + Constants.PAGE_REDIRECT + "&msg="
        + "info.registration.successfull";
  }


  public String getIncome() {
    return income;
  }


  public void setIncome(String income) {
    this.income = income;
  }


  public String getPasswordMatch() {
    return passwordMatch;
  }

  public void setPasswordMatch(String passwordMatch) {
    this.passwordMatch = passwordMatch;
  }
  
  public void handleKeyEvent() {
    
}

}
