package com.semsoftware.core.controllers.user;


import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import com.semsoftware.core.util.LogHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;

import com.semsoftware.core.controllers.NavigationBean;
import com.semsoftware.core.dto.UserDTO;
import com.semsoftware.core.facade.UserFacade;
import com.semsoftware.core.util.HttpUtil;

/**
 * @author ozdesimsek
 */
@Named("userBean")
@Scope("session")
public class UserBean implements Serializable{

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  private static final Logger LOGGER = LoggerFactory.getLogger(UserBean.class);

  @Inject
  private NavigationBean navigation;
  @Inject
  private App app;


  private UserDTO currentUser;


  @PostConstruct
  public void init() {
    currentUser = new UserDTO();
  }

  public String logout() {
    HttpSession session = HttpUtil.getSession();
    LOGGER.info(LogHelper.getMsgHeadUsr() + "LOG OUT");
    session.invalidate();
    app.removeOnlineUser(currentUser);
    this.currentUser = null;
    return navigation.getPageIndex();
  }


  public UserDTO getCurrentUser() {
    System.out.println(currentUser.getRole());
    return currentUser;
  }

  public void setCurrentUser(UserDTO currentUser) {
    this.currentUser = currentUser;
    System.out.println(currentUser.getRole());
  }


}
