package com.semsoftware.core.controllers;


import com.semsoftware.core.controllers.user.App;
import com.semsoftware.core.dto.BookingDTO;
import com.semsoftware.core.dto.DataType;
import com.semsoftware.core.dto.RoomDTO;
import com.semsoftware.core.facade.BookingFacade;
import com.sun.prism.PixelFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named("searchBean")
@Scope("session")
public class SearchBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SearchBean.class);

    @Inject
    private NavigationBean navigation;
    @Inject
    private App app;

    private String searchId;
    private DataType filter;
    private Boolean showTable = false;
    private Boolean showHintNoRes = false;
    @Inject
    private BookingFacade bookingFacade;
    private List<BookingDTO> bookings;

    @PostConstruct
    public void init() {
        filter = DataType.BOOKING;
        bookings = new ArrayList<>();
    }

    /**
     * Is called when search-btn is clicked
     */
    public void search() {
        switch (filter) {
            case BOOKING:
                bookings.clear();
                BookingDTO foundDTO = bookingFacade.getBookingOnId(Integer.valueOf(searchId));
                if (foundDTO != null)
                    bookings.add(foundDTO);
                // show dto or no-result-hint
                if (bookings.isEmpty()) {
                    showHintNoRes = true;
                    showTable = false;
                } else {
                    showHintNoRes = false;
                    showTable = true;
                }
                break;
            // later it should be possible to search for other types too
        }
    }

    public String getSearchId() {
        return searchId;
    }

    public void setSearchId(String searchId) {
        this.searchId = searchId;
    }

    public DataType[] getDataTypes() {
        return DataType.values();
    }

    public DataType getFilter() {
        return filter;
    }

    public void setFilter(DataType filter) {
        this.filter = filter;
    }

    public List<BookingDTO> getBookings() {
        return bookings;
    }

    public void setBookings(List<BookingDTO> bookings) {
        this.bookings = bookings;
    }

    public Boolean getShowHintNoRes() {
        return showHintNoRes;
    }

    public void setShowHintNoRes(Boolean showHintNoRes) {
        this.showHintNoRes = showHintNoRes;
    }

    public Boolean getShowTable() {
        return showTable;
    }

    public void setShowTable(Boolean showTable) {
        this.showTable = showTable;
    }
}
