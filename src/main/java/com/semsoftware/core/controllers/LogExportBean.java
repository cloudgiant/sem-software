package com.semsoftware.core.controllers;

import com.semsoftware.core.dto.LogDTO;
import com.semsoftware.core.facade.LogFacade;
import com.semsoftware.core.util.LogHelper;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.context.annotation.Scope;

import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by Martina/Tatiana on 08.01.2016.
 */
@ManagedBean
@Named("logExport")
@Scope("session")
public class LogExportBean {

    private static final String FILE_NAME_ALL = "logs_all.html";
    private static final String FILE_NAME_USER = "logs_user.html";
    SimpleDateFormat date_format = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss");
    private StreamedContent fileAll;
    private StreamedContent fileUser;
    @Inject
    private LogFacade logFacade;

    public StreamedContent exportLogsAll() {
        String pathToDocument = null;

        try {
            generateHtml(false, FILE_NAME_ALL);
            pathToDocument = new File(FILE_NAME_ALL).getAbsolutePath();
            InputStream stream = new FileInputStream(pathToDocument);
            fileAll = new DefaultStreamedContent(stream, "text/html", FILE_NAME_ALL);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileAll;
    }

    public StreamedContent exportLogsUser() {
        String pathToDocument = null;

        try {
            generateHtml(true, FILE_NAME_USER);
            pathToDocument = new File(FILE_NAME_USER).getAbsolutePath();
            InputStream stream = new FileInputStream(pathToDocument);
            fileUser = new DefaultStreamedContent(stream, "text/html", FILE_NAME_USER);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileUser;
    }

    private void generateHtml(boolean onlyUserAction, String fileName) {
        List<LogDTO> logList = logFacade.findAllLogs();
        Writer writer = null;

        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(fileName), "utf-8"));
            writer.write(getHead());
            for (LogDTO log : logList) {
                if (onlyUserAction) {
                    if (log.getMessage().startsWith(LogHelper.USERACTION)) {
                        log.setMessage(log.getMessage().replace(LogHelper.USERACTION, ""));
                        writer.write(format(log));
                    }
                } else {
                    writer.write(format(log));
                }
            }
            writer.write(getTail());
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }


    // this method is called just after the handler using this
    // formatter is created
    public String getHead() {
        return "<!DOCTYPE html>\n<head>\n<style " + "type=\"text/css\">\n"
                + "table { width: 100% }\n" + "th { font:bold 10pt Tahoma; }\n"
                + "td { font:normal 10pt Tahoma; vertical-align:top; }\n"
                + "p { margin-left: 40px; margin-top: 0px; margin-bottom: 0px; }\n"
                + "h1 {font:normal 11pt Tahoma;}\n" + "</style>\n"
                + "</head>\n" + "<body>\n" + "<h1>" + (new Date()) + "</h1>\n"
                + "<table border=\"0\" cellpadding=\"5\" cellspacing=\"3\">\n"
                + "<tr align=\"left\">\n"
                + "\t<th style=\"width:10%\">Loglevel</th>\n"
                + "\t<th style=\"width:15%\">Time</th>\n"
                + "\t<th style=\"width:15%\">Class</th>\n"
                + "\t<th style=\"width:60%\">Log Message</th>\n" + "</tr>\n";
    }

    public String getTail() {
        return "</table>\n</body>\n</html>";
    }

    public String format(LogDTO log) {
        StringBuffer buf = new StringBuffer(1000);
        buf.append("<tr>\n");

        Level level = Level.ALL;
        try {
            level = Level.parse(log.getLevel());
        } catch (Exception e) {
            //
        }
        // colorize any levels >= WARNING in red
        if (level.intValue() >= Level.WARNING.intValue()) {
            buf.append("\t<td style=\"color:red\">");
            buf.append("<b>");
            buf.append(level);
            buf.append("</b>");
        } else {
            buf.append("\t<td>");
            buf.append(level);
        }

        buf.append("</td>\n");
        buf.append("\t<td>");
        buf.append(date_format.format(log.getDate()));
        buf.append("</td>\n");
        buf.append("\t<td>");
        buf.append(log.getLogger());
        buf.append("</td>\n");
        buf.append("\t<td>");
        buf.append(log.getMessage());
        buf.append("</td>\n");
        buf.append("</tr>\n");

        return buf.toString();
    }
}
