package com.semsoftware.core.controllers;


import com.semsoftware.core.controllers.user.App;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import java.io.Serializable;

/**
 * @author ozdesimsek
 */
@Named("receiptBean")
@Scope("session")
public class ReceiptBean implements Serializable{

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  private static final Logger LOGGER = LoggerFactory.getLogger(ReceiptBean.class);

  @Inject
  private NavigationBean navigation;
  @Inject
  private App app;

  @PostConstruct
  public void init() {
    LOGGER.info("Bean created");
  }
  public String showReceiptList() {

    return navigation.getPageReceiptList();
  }
}
