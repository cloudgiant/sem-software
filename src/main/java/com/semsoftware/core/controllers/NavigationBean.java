package com.semsoftware.core.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import java.io.Serializable;

/**
 * 
 * @author ozdesimsek
 *
 */
@Named("navigation")
@Scope("session")
public class NavigationBean implements Serializable{

  private static final Logger logger = LoggerFactory.getLogger(NavigationBean.class);

  private final static String PAGE_INDEX = "/public/index";
  private final static String PAGE_HOME = "/pages/main";

  private final static String PAGE_REGISTER = "/public/user/register";

  private final static String PAGE_SEARCH = "/pages/search";
  private final static String PAGE_SEARCH_RESULT = "/pages/search/result";

  private final static String PAGE_RECEIPT_LIST = "/pages/receipts/list";
  private final static String PAGE_BOOKING_LIST = "/pages/bookings/listBooking";
  private final static String PAGE_CLIENT_LIST = "/pages/clients/list";

  private final static String PAGE_CREATE_CLIENT = "/pages/clients/createClient";
  private final static String PAGE_CREATE_BOOKING = "/pages/bookings/createBooking";
  
  private final static String PAGE_ROOMS_LIST = "/pages/rooms/list";


  @PostConstruct
  public void init() {
    logger.info("Bean created");
  }

  public String getPageIndex() {
    return  PAGE_INDEX;
  }

  public String getPageHome() {
    return  PAGE_HOME;
  }

  public String getPageRegister() {
    return  PAGE_REGISTER;
  }

  public String getPageReceiptList() {
    return PAGE_RECEIPT_LIST;
  }

  public String getPageBookingList() {
    return PAGE_BOOKING_LIST;
  }

  public String getPageClientList() {
    return PAGE_CLIENT_LIST;
  }

  public String getPageCreateClient() {
    return PAGE_CREATE_CLIENT;
  }

  public String getPageSearch() {
    return PAGE_SEARCH;
  }

  public String getPageSearchResult() {
    return PAGE_SEARCH_RESULT;
  }
  
  public String getPageCreateBooking(){
    return PAGE_CREATE_BOOKING;
  }

  public String getPageRoomsList() {
    return PAGE_ROOMS_LIST;
  }
}
