package com.semsoftware.core.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;

import com.semsoftware.core.dto.BookingDTO;
import com.semsoftware.core.facade.BookingFacade;


@Named("bookingList")
@Scope("view")
public class BookingListBean {
  
  private static final Logger logger = LoggerFactory.getLogger(BookingListBean.class);
  
  @Inject
  private BookingFacade bookingFacade;
  private List<BookingDTO> bookings = new ArrayList<BookingDTO>();
  private boolean cancel;
  public BookingListBean(){}
  
  @PostConstruct
  public void init() {
    logger.info("CREATE NEW BEAN");
    cancel = true;
    setBookings(bookingFacade.findActiveBookings());
  }

  public List<BookingDTO> getBookings() {
    return bookings;
  }

  public void setBookings(List<BookingDTO> bookings) {
    this.bookings = bookings;
  }

  public boolean isCancel() {
    return cancel;
  }

  public void setCancel(boolean cancel) {
    this.cancel = cancel;
  }
  
  public void loadBookings(){
    bookings = new ArrayList<BookingDTO>();
    if(cancel == true){
      bookings = bookingFacade.findActiveBookings();
      System.out.println("Aktive:"+bookings.size());
    }else{
      setBookings(bookingFacade.findCanceledBookings());
      System.out.println("Stornierte:"+bookings.size());
    }
  }
}
