package com.semsoftware.core.controllers;


import com.semsoftware.core.controllers.user.App;
import com.semsoftware.core.dto.RoomDTO;
import com.semsoftware.core.facade.RoomFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * @author ozdesimsek
 */
@Named("roomBean")
@Scope("session")
public class RoomBean implements Serializable{

  @Inject
  RoomFacade facade;

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  private static final Logger LOGGER = LoggerFactory.getLogger(RoomBean.class);

  @Inject
  private NavigationBean navigation;
  @Inject
  private App app;
  private List<RoomDTO> rooms;

  @PostConstruct
  public void init() {
    LOGGER.info("Bean created");

    if (rooms == null) {
      rooms = facade.findAll();
    }
  }


  public List<RoomDTO> getRooms() {
    return rooms;
  }
}
