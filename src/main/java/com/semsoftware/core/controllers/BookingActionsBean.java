package com.semsoftware.core.controllers;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.semsoftware.core.dto.BookingDTO;
import com.semsoftware.core.facade.BookingFacade;
import com.semsoftware.core.util.Constants;

@Named("bookingActions")
@Scope("session")
public class BookingActionsBean {

  @Inject
  private NavigationBean navigation;
  @Inject
  private BookingFacade facade;

  @PostConstruct
  public void init() {

  }

  public String editBooking(final BookingDTO bookingDTO, String returnPage) {
    return navigation.getPageCreateBooking() + Constants.PAGE_REDIRECT + "&bookingId=" + bookingDTO.getId()
        + "&returnPage=" + returnPage;
  }
  
  public String cancelBooking(final BookingDTO bookingDTO, String returnPage) {
    bookingDTO.setCancel(true);
    facade.saveBooking(bookingDTO);
    return returnPage + Constants.PAGE_REDIRECT;
  }
}
