package com.semsoftware.core.facade;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.semsoftware.core.dto.UserDTO;


/**
 * Central access point for service logic which is associated with users.
 * e.g. persisting, updating and searching users in the database.
 * In case of implementing a REST-API in the future its methods will be bounded on the facade-interfaces
 * @author ozdesimsek
 */
@Transactional
public interface UserFacade {

  /**
   * Persists a new user in the database
   * 
   * @param user user object (UserDTO) wich should be stored
   * @throws NullPointerException if the passed parameter is null
   */
  UserDTO createUser(UserDTO user);

  /**
   * Returns a List of all users
   * 
   * @return a list with all the users from the data base
   */
  List<UserDTO> getAllUsers();


  /**
   * Returns a loged in user (as UserDTO) after login with email and password
   * 
   * @param email email of the user to login
   * @param password password of the user to login
   * @return the user (as UserDTO) who have been loged in
   */
  UserDTO login(String email, String password);

  /**
   * Check whether the given username and e-mail address are uniyue
   * 
   * @param userName userName of the user to login
   * @param email email of the user to login
   * @return TRUE in case the data is unique, otherwise FALSE
   */
  boolean verifyUnique(String userName, String email);

  /**
   * Search for a user with the specfied e-mail address
   * 
   * @param email email of the user to search in the data base
   * @return the user (as UserDTO) in case the given e-mail was found
   */
  UserDTO findByEmail(String email);

 /**
   * Search for a user with the specfied ID
   * 
   * @param userId ID of the user to search in the data base
   * @return the user (as UserDTO) in case the given ID was found
   */
  UserDTO findById(Integer userId);

  /**
   * Updates an user. If the value is null the database will remain unchanged
   * 
   * @param edituser user object (as UserDTO) to be updated
   */
  void updateUser(UserDTO edituser);

  /**
   * Returns a List of all ADMIN users 
   * @return a list with all the ADMIN users from the data base
   */
  List<UserDTO> findAllAdmins();

  /**
   * ??? Search for a user with the specfied e-mail address or username
   * 
   * @param unameMail ??? username or email of the user to search in the data base
   * @return the user (as UserDTO) in case the given e-mail was found
   */
  UserDTO findByUserNameOrMail(String unameMail);


}
