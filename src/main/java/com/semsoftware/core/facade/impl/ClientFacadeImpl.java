package com.semsoftware.core.facade.impl;

import com.semsoftware.core.dto.ClientDTO;
import com.semsoftware.core.facade.ClientFacade;
import com.semsoftware.core.service.ClientService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
/**
 * 
 * @author ozdesimsek
 *
 */

@Named
public class ClientFacadeImpl implements ClientFacade {

  @Inject
  private ClientService clientService;
  
  @Override
  public List<ClientDTO> getAllClientDTOs() {
    return clientService.getAllClientDTOs();
  }

}
