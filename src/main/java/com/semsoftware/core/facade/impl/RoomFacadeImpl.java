package com.semsoftware.core.facade.impl;

import com.semsoftware.core.dto.BookingDTO;
import com.semsoftware.core.dto.RoomDTO;
import com.semsoftware.core.facade.BookingFacade;
import com.semsoftware.core.facade.RoomFacade;
import com.semsoftware.core.service.BookingService;
import com.semsoftware.core.service.RoomService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
public class RoomFacadeImpl implements RoomFacade {

  @Inject
  private RoomService roomService;
  
  @Override
  public List<RoomDTO> findAll() {
    return roomService.findAll();
  }

}