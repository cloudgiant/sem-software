package com.semsoftware.core.facade.impl;

import com.semsoftware.core.dto.LogDTO;
import com.semsoftware.core.facade.LogFacade;
import com.semsoftware.core.service.LogService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Created by Martina on 08.01.2016.
 */
@Named
public class LogFacadeImpl implements LogFacade {

    @Inject
    private LogService logService;

    @Override
    public List<LogDTO> findAllLogs() {
        return logService.findAllLogs();
    }
}
