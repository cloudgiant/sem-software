package com.semsoftware.core.facade.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import com.semsoftware.core.dto.UserDTO;
import com.semsoftware.core.facade.UserFacade;
import com.semsoftware.core.service.UserService;
import com.semsoftware.core.util.HttpUtil;
import com.semsoftware.core.util.LogHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author ozdesimsek
 *
 */
@Named
public class UserFacadeImpl implements UserFacade {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserFacadeImpl.class);

  @Inject
  private UserService userService;

  @Override
  public UserDTO createUser(final UserDTO user) {
    LOGGER.info(LogHelper.getMsgHeadUsr() + " created new user " + user.getFirstName() + " " + user.getLastName());
    return userService.createUser(user);
  }

  public List<UserDTO> getAllUsers() {
    return userService.getAllUsersDTO();
  }

  @Override
  public UserDTO login(final String email, final String password) {
    return userService.login(email, password);
  }

  @Override
  public boolean verifyUnique(final String userName, final String email) {
    return userService.verifyUnique(userName, email);
  }

  @Override
  public UserDTO findByEmail(final String email) {
    return userService.findByEmail(email);
  }


  @Override
  public UserDTO findById(final Integer userId) {
    return userService.findById(userId);
  }

  public void updateUser(final UserDTO user) {
    userService.updateUser(user);
  }

  @Override
  public List<UserDTO> findAllAdmins() {
    return userService.findAllAdmins();
  }

  @Override
  public UserDTO findByUserNameOrMail(final String userNameOrEmail) {
    return userService.findByUserNameOrMail(userNameOrEmail);
  }



}
