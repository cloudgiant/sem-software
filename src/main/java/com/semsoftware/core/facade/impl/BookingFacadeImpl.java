package com.semsoftware.core.facade.impl;

import com.semsoftware.core.dto.BookingDTO;
import com.semsoftware.core.dto.ClientDTO;
import com.semsoftware.core.dto.RoomDTO;
import com.semsoftware.core.facade.BookingFacade;
import com.semsoftware.core.service.BookingService;
import com.semsoftware.core.util.HttpUtil;
import com.semsoftware.core.util.LogHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@Named
public class BookingFacadeImpl implements BookingFacade {

  private static final Logger LOGGER = LoggerFactory.getLogger(BookingFacadeImpl.class);

  @Inject
  private BookingService bookingService;
  
  @Override
  public List<BookingDTO> findAll() {
    return bookingService.findAll();
  }

  @Override
  public void saveBooking(BookingDTO bookingDTO) {
    if(bookingDTO.getId()==null){
      System.out.println("create Booking");
      LOGGER.info(LogHelper.getMsgHeadUsr() + " created new booking ");
      bookingService.createBooking(bookingDTO);
    }else{
      System.out.println("edit Booking");
      LOGGER.info(LogHelper.getMsgHeadUsr() + " edited booking ");
      bookingService.editBooking(bookingDTO);
    }
  }

  @Override
  public List<ClientDTO> getAllClientsForBooking(Integer id) {
    return bookingService.getAllClientsForBooking(id);
  }

  @Override
  public RoomDTO getRoomForBooking(Integer id) {
    return bookingService.getRoomForBooking(id);
  }

  @Override
  public RoomDTO getBookedRoomForClient(ClientDTO client) {
    return bookingService.getBookedRoomForClient(client);
  }

  @Override
  public List<BookingDTO> getAllBookingsOnDate(Date date) {
    return bookingService.getAllBookingsOnDate(date);
  }

  @Override
  public List<BookingDTO> getAllBookingsForClient(ClientDTO client) {
    return bookingService.getAllBookingsForClient(client);
  }

  @Override
  public List<BookingDTO> getAllBookingsInPeriod(Date start, Date end) {
    return bookingService.getAllBookingsInPeriod(start, end);
  }

  @Override
  public List<BookingDTO> getAllBookingsWithDiscount() {
    return bookingService.getAllBookingsWithDiscount();
  }

  @Override
  public BookingDTO getBookingOnId(Integer id) {
    return bookingService.getBookingOnId(id);
  }

  @Override
  public List<BookingDTO> findCanceledBookings() {
    return bookingService.findCanceledBookings();
  }

  @Override
  public List<BookingDTO> findActiveBookings() {
    return bookingService.findActiveBookings();
  }

  @Override
  public List<BookingDTO> findBookingsBetweenDates(Date from, Date to) {
    return bookingService.findBookingsBetweenDates(from, to);
  }
}
