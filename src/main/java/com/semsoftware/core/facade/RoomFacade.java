package com.semsoftware.core.facade;

import com.semsoftware.core.dto.RoomDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface RoomFacade {
  
  public List<RoomDTO> findAll();

}