package com.semsoftware.core.facade;

import com.semsoftware.core.dto.LogDTO;

import java.util.List;

/**
 * Created by Martina on 08.01.2016.
 */
public interface LogFacade {

    public List<LogDTO> findAllLogs();
}
