package com.semsoftware.core.facade;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.semsoftware.core.dto.ClientDTO;

@Transactional
public interface ClientFacade {
  public List<ClientDTO> getAllClientDTOs();
}
