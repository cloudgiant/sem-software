package com.semsoftware.core.service;

import com.semsoftware.core.dto.RoomDTO;

import java.util.List;

public interface RoomService {

  public List<RoomDTO> findAll();

}