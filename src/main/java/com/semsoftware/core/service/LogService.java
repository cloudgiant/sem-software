package com.semsoftware.core.service;

import com.semsoftware.core.dto.LogDTO;

import java.util.List;

/**
 * Created by Martina on 06.01.2016.
 */
public interface LogService {

    public List<LogDTO> findAllLogs();
}
