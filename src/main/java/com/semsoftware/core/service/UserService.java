package com.semsoftware.core.service;

import java.util.List;
import java.util.Set;

import com.semsoftware.core.dto.UserDTO;
import com.semsoftware.core.model.User;

public interface UserService {
  
  /**
   * Create new user entity based on provided information from userDTO
   *
   * @param userDTO {@link UserDTO}
   */
  UserDTO createUser(UserDTO userDTO);


  /**
   * Returns a List of all users
   * 
   * @return a list with all the users from the data base
   */
  List<User> getAllUsers();

  /**
   * Returns a loged in user (as UserDTO) after login with email and password
   * 
   * @param email email of the user to login
   * @param password password of the user to login
   * @return the user (as UserDTO) who have been loged in
   */
  UserDTO login(String email, String password);

  /**
   * Check whether the given username and e-mail address are uniyue
   * 
   * @param userName userName of the user to login
   * @param email email of the user to login
   * @return TRUE in case the data is unique, otherwise FALSE
   */
  boolean verifyUnique(String username, String email);

  /**
   * Search for a user with the specfied ID
   * 
   * @param userId ID of the user to search in the data base
   * @return the user in case the given ID was found
   */
  User findUserById(Integer id);

  /**
   * Returns a List of all users
   * 
   * @return a list with all the users (as UserDTOs) from the data base
   */
  List<UserDTO> getAllUsersDTO();


  /**
   * Search for a user with the specfied e-mail address
   * 
   * @param email email of the user to search in the data base
   * @return the user (as UserDTO) in case the given e-mail was found
   */
  UserDTO findByEmail(String email);

  /**
   * Search for a user with the specfied ID
   * 
   * @param userId ID of the user to search in the data base
   * @return the user (as UserDTO) in case the given ID was found
   */
  UserDTO findById(Integer userId);

  /**
   * Updates an user. If the value is null the database will remain unchanged
   * 
   * @param edituser user object (as UserDTO) to be updated
   */
  void updateUser(UserDTO user);

  List<UserDTO> findAllAdmins();

  /**
   * ??? Search for a user with the specfied e-mail address or username
   * 
   * @param unameMail ??? username or email of the user to search in the data base
   * @return the user (as UserDTO) in case the given e-mail was found
   */
  UserDTO findByUserNameOrMail(String unameMail);

}
