package com.semsoftware.core.service.impl;

import com.semsoftware.core.dto.BookingDTO;
import com.semsoftware.core.dto.ClientDTO;
import com.semsoftware.core.dto.RoomDTO;
import com.semsoftware.core.model.Booking;
import com.semsoftware.core.repository.BookingRepository;
import com.semsoftware.core.service.BookingService;
import com.semsoftware.core.service.EntityConverter;

import javax.inject.Inject;
import javax.inject.Named;

import java.util.Date;
import java.util.List;

@Named
public class BookingServiceImpl implements BookingService {

  @Inject
  private BookingRepository bookingRepository;
  @Inject
  private EntityConverter factory;
  
  @Override
  public List<BookingDTO> findAll() {
    return bookingRepository.findAllBookingDTOs();
  }

  @Override
  public void createBooking(BookingDTO bookingDTO) {

    final Booking booking = factory.createBooking(bookingDTO);

    bookingRepository.save(booking);
    
  }

  @Override
  public List<ClientDTO> getAllClientsForBooking(Integer id) {
    return bookingRepository.findAllClientsForBooking(id);
  }

  @Override
  public RoomDTO getRoomForBooking(Integer id) {
    return bookingRepository.findRoomForBooking(id);
  }

  @Override
  public RoomDTO getBookedRoomForClient(ClientDTO client) {
    return bookingRepository.findBookedRoomForClient(client);
  }

  @Override
  public List<BookingDTO> getAllBookingsOnDate(Date date) {
    return bookingRepository.findAllBookingsOnDate(date);
  }

  @Override
  public List<BookingDTO> getAllBookingsForClient(ClientDTO client) {
    return bookingRepository.findAllBookingsForClient(client);
  }

  @Override
  public List<BookingDTO> getAllBookingsInPeriod(Date start, Date end) {
    return bookingRepository.findAllBookingsInPeriod(start, end);
  }

  @Override
  public List<BookingDTO> getAllBookingsWithDiscount() {
    return bookingRepository.findAllBookingsWithDiscount();
  }

  @Override
  public BookingDTO getBookingOnId(Integer id) {
    return bookingRepository.findBookingOnId(id);
  }

  @Override
  public void editBooking(BookingDTO bookingDTO) {
    Booking booking = factory.createBooking(bookingDTO);
    bookingRepository.merge(booking);
  }

  @Override
  public List<BookingDTO> findCanceledBookings() {
    return bookingRepository.findCanceledBookings();
  }

  @Override
  public List<BookingDTO> findActiveBookings() {
    return bookingRepository.findActiveBookings();
  }

  @Override
  public List<BookingDTO> findBookingsBetweenDates(Date from, Date to) {
    return bookingRepository.findBookingsBetweenDates(from, to);
  }

}
