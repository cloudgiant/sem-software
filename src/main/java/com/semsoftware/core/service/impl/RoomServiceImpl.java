package com.semsoftware.core.service.impl;

import com.semsoftware.core.dto.RoomDTO;
import com.semsoftware.core.repository.RoomRepository;
import com.semsoftware.core.service.RoomService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
public class RoomServiceImpl implements RoomService {

  @Inject
  private RoomRepository roomRepository;
  
  @Override
  public List<RoomDTO> findAll() {
    return roomRepository.findAllRoomDTOs();
  }

}