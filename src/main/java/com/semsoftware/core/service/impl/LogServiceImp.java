package com.semsoftware.core.service.impl;

import com.semsoftware.core.dto.LogDTO;
import com.semsoftware.core.repository.LogRepository;
import com.semsoftware.core.service.LogService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Created by Martina on 06.01.2016.
 */
@Named
public class LogServiceImp implements LogService {

    @Inject
    private LogRepository logRepository;


    @Override
    public List<LogDTO> findAllLogs() {
        return logRepository.findAllLogs();
    }
}
