package com.semsoftware.core.service.impl;

import com.semsoftware.core.dto.ClientDTO;
import com.semsoftware.core.model.Client;
import com.semsoftware.core.repository.ClientRepository;
import com.semsoftware.core.service.ClientService;
import com.semsoftware.core.service.EntityConverter;
import com.semsoftware.core.util.LogHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
public class ClientServiceImpl implements ClientService {

  private static final Logger LOGGER = LoggerFactory.getLogger(ClientServiceImpl.class);

  @Inject
  private ClientRepository clientRepository;
  @Inject
  private EntityConverter entityConverter;

  @Override
  public List<ClientDTO> getAllClientDTOs() {
    return clientRepository.getAllClientDTOs();
  }

  @Override
  public void createClient(ClientDTO clientDTO) {
    Client client = entityConverter.createClient(clientDTO);
    LOGGER.info(LogHelper.getMsgHeadUsr() + "create new client " + clientDTO.getFirstName() + " " + clientDTO.getLastName());
    clientRepository.save(client);
  }


}
