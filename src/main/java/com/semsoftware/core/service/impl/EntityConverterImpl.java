package com.semsoftware.core.service.impl;

import com.semsoftware.core.dto.*;
import com.semsoftware.core.model.*;
import com.semsoftware.core.service.EntityConverter;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
/**
 * 
 * @author ozdesimsek
 *
 */
@Named
public class EntityConverterImpl implements EntityConverter {
  
  @Override
  public User createUser(UserDTO userDto) {
    final User user = new User();
    user.setId(userDto.getId());
    user.setFirstName(userDto.getFirstName());
    user.setLastName(userDto.getLastName());
    user.setUserName(userDto.getUserName());
    user.setPassword(userDto.getPassword());
    user.setSalt(userDto.getSalt());
    user.setEmail(userDto.getEmail());
    user.setBirthDate(userDto.getBirthDate());
    user.setRole(userDto.getRole());

    return user;
  }

  @Override
  public Booking createBooking(BookingDTO bookingDto) {
    final Booking booking = new Booking();
    booking.setId(bookingDto.getId());
    List<ClientDTO> cl = bookingDto.getClients();
    if (cl != null) {

      List<Client> cl2 = new ArrayList<Client>();
      for (ClientDTO c : cl) {
        cl2.add(createClient(c));
      }
      booking.setClients(cl2);
    }
    List<RoomDTO> rl = bookingDto.getRooms();
    if (rl != null) {
      List<Room> rl2 = new ArrayList<Room>();
      for (RoomDTO r : rl) {
        rl2.add(createRoom(r));
      }
      booking.setRooms(rl2);
    }
    booking.setDateFrom(bookingDto.getDateFrom());
    booking.setDateTo(bookingDto.getDateTo());
    booking.setPrice(bookingDto.getPrice());
    booking.setDuration(bookingDto.getDuration());
    booking.setCancel(bookingDto.getCancel());

    return booking;
  }

  @Override
  public Client createClient(ClientDTO c) {
    final Client client = new Client();
    client.setId(c.getId());
    client.setFirstName(c.getFirstName());
    client.setLastName(c.getLastName());
    client.setEmail(c.getEmail());
    client.setBirthDate(c.getBirthDate());
    client.setGender(c.getGender());
    client.setCompanyName(c.getCompanyName());
//    client.setDiscount(c.getDiscount());
    client.setTelephone(c.getTelephone());
//    client.setWeb(c.getWeb());
//    client.setFax(c.getFax());
    client.setAddress(c.getAddress());
    client.setNote(c.getNote());
    return client;
  }

  @Override
  public Room createRoom(RoomDTO r) {
    final Room room = new Room();
    room.setId(r.getId());
    room.setName(r.getName());
    room.setMaxPeople(r.getMaxPeople());
    room.setPriceMax(r.getPriceMax());
    room.setPriceForChild(r.getPriceForChild());
    room.setPriceSingle(r.getPriceSingle());
    room.setSize(r.getSize());
    room.setFacilities(r.getFacilities());
    List<BookingDTO> bl = r.getBookings();
    if (bl != null || !bl.isEmpty()) {
      List<Booking> bl2 = new ArrayList<Booking>();
      for (BookingDTO b : bl) {
        bl2.add(createBooking(b));
      }
      room.setBookings(bl2);
    }
    List<InvoiceDTO> il = r.getInvoices();
    if (il != null || !il.isEmpty()) {
      List<Invoice> il2 = new ArrayList<Invoice>();
      for (InvoiceDTO i : il) {
        il2.add(createInvoice(i));
      }
      room.setInvoices(il2);
    }
    return room;
  }

  @Override
  public Invoice createInvoice(InvoiceDTO i) {
    Invoice invoice = new Invoice();
    invoice.setAddressClient(i.getAddressClient());
    invoice.setAddressHotel(i.getAddressHotel());
    List<ClientDTO> cl = i.getClients();
    List<Client> cl2 = new ArrayList<Client>();
    for (ClientDTO c : cl) {
      cl2.add(createClient(c));
    }
    invoice.setClients(cl2);
    invoice.setCreatedOn(i.getCreatedOn());
    invoice.setDuration(i.getDuration());
    invoice.setPrice(i.getPrice());
    List<RoomDTO> rl = i.getRooms();
    List<Room> rl2 = new ArrayList<Room>();
    for (RoomDTO r : rl) {
      rl2.add(createRoom(r));
    }
    invoice.setRooms(rl2);
    invoice.setText(i.getText());
    return invoice;
  }

}
