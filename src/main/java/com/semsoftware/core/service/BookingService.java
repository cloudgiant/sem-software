package com.semsoftware.core.service;

import com.semsoftware.core.dto.BookingDTO;
import com.semsoftware.core.dto.ClientDTO;
import com.semsoftware.core.dto.RoomDTO;

import java.util.Date;
import java.util.List;

public interface BookingService {

  public List<BookingDTO> findAll();

  public void createBooking(BookingDTO bookingDTO);

  public List<ClientDTO> getAllClientsForBooking(Integer id);

  public RoomDTO getRoomForBooking(Integer id);

  public RoomDTO getBookedRoomForClient(ClientDTO client);

  public List<BookingDTO> getAllBookingsOnDate(Date date);

  public List<BookingDTO> getAllBookingsForClient(ClientDTO client);

  public List<BookingDTO> getAllBookingsInPeriod(Date start, Date end);

  public List<BookingDTO> getAllBookingsWithDiscount();

  public BookingDTO getBookingOnId(Integer id);

  public void editBooking(BookingDTO bookingDTO);
  
  public List<BookingDTO> findCanceledBookings();
  
  public List<BookingDTO> findActiveBookings();
  
  public List<BookingDTO> findBookingsBetweenDates(Date from, Date to);

}
