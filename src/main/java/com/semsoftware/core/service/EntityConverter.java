package com.semsoftware.core.service;

import com.semsoftware.core.dto.*;
import com.semsoftware.core.model.*;

/**
 * 
 * @author ozdesimsek
 *
 */
public interface EntityConverter {

  

  /**
   * Converts a UserDTO into a User with equal attributes
   * 
   * @param userDto dto to convert
   * @return respective Entity representation
   * @throws NullPointerExcpetion if the passed parameter is null
   */
  User createUser(UserDTO userDto);

  Booking createBooking(BookingDTO bookingDTO);

  Client createClient(ClientDTO c);

  Room createRoom(RoomDTO r);

  Invoice createInvoice(InvoiceDTO i);

  
}

