package com.semsoftware.core.service;

import com.semsoftware.core.dto.BookingDTO;
import com.semsoftware.core.dto.ClientDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface ClientService {

  public List<ClientDTO> getAllClientDTOs();

  public void createClient(ClientDTO clientDTO);
}
