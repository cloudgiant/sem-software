package com.semsoftware.core.model;

import javax.persistence.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "room")
public class Room implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "max_people")
    private Integer maxPeople;

    @Column(name = "price_single")
    private Double priceSingle;

    @Column(name = "price_max")
    private Double priceMax;

    @Column(name = "price_for_child")
    private Double priceForChild;

    @Column(name = "facilities")
    private String facilities;

    @Column(name = "size")
    private Integer size;

    @ManyToMany(mappedBy = "rooms", cascade=CascadeType.MERGE, fetch=FetchType.EAGER)
    private List<Booking> bookings = new ArrayList<Booking>();

    @ManyToMany(mappedBy = "rooms")
    private List<Invoice> invoices = new ArrayList<Invoice>();

    /**
     * default constructor for hibernate
     */
    public Room() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMaxPeople() {
        return maxPeople;
    }

    public void setMaxPeople(Integer maxPeople) {
        this.maxPeople = maxPeople;
    }

    public Double getPriceSingle() {
        return priceSingle;
    }

    public void setPriceSingle(Double priceSingle) {
        this.priceSingle = priceSingle;
    }

    public Double getPriceMax() {
        return priceMax;
    }

    public void setPriceMax(Double priceMax) {
        this.priceMax = priceMax;
    }

    public Double getPriceForChild() {
        return priceForChild;
    }

    public void setPriceForChild(Double priceForChild) {
        this.priceForChild = priceForChild;
    }

    public List<Booking> getBookings() {
        return bookings;
    }


    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }


    public List<Invoice> getInvoices() {
        return invoices;
    }


    public void setInvoices(List<Invoice> invoices) {
        this.invoices = invoices;
    }

    public String getFacilities() {
        return facilities;
    }

    public void setFacilities(String facilities) {
        this.facilities = facilities;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
