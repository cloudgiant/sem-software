package com.semsoftware.core.model;

import javax.persistence.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "booking")
public class Booking implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToMany(cascade=CascadeType.MERGE)
    @JoinTable(name = "client_booking_mm", joinColumns = @JoinColumn(name = "booking_id"),
            inverseJoinColumns = @JoinColumn(name = "client_id"))
    private List<Client> clients = new ArrayList<Client>();

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "room_booking_mm", joinColumns = @JoinColumn(name = "booking_id"),
            inverseJoinColumns = @JoinColumn(name = "room_id"))
    private List<Room> rooms = new ArrayList<Room>();

    @Column(name = "price")
    private Double price;

    // in days
    @Column(name = "duration")
    private Integer duration;
    
    @Column(name = "dateFrom")
    private Date dateFrom;
    
    @Column(name = "dateTo")
    private Date dateTo;
    
    @Column(name = "cancel")
    private Boolean cancel;


    /**
     * default constructor for hibernate
     */
    public Booking() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Date getDateFrom() {
      return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
      this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
      return dateTo;
    }

    public void setDateTo(Date dateTo) {
      this.dateTo = dateTo;
    }


    public Boolean getCancel() {
      return cancel;
    }


    public void setCancel(Boolean cancel) {
      this.cancel = cancel;
    }
    
    
    
}
