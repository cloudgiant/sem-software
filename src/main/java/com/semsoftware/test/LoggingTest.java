package com.semsoftware.test;

import com.semsoftware.core.repository.LogRepository;
import com.semsoftware.core.repository.impl.LogRepositoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

/**
 * Created by Martina on 08.01.2016.
 */
public class LoggingTest {

    private LogRepository log;

    @Before
    public void setUp() throws SQLException {
        log = new LogRepositoryImpl();
    }

    @Test
    public void testLogList() {
        Assert.assertNotNull(log.findAllLogs());
    }
}
