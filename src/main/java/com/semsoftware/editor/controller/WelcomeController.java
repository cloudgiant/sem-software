package com.semsoftware.editor.controller;

import org.springframework.context.annotation.Scope;

import javax.inject.Named;


/**
 * Created by steph on 11/18/15.
 */
@Named("welcomeController")
@Scope("view")
public class WelcomeController {
}
