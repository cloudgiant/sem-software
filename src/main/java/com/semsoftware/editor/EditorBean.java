package com.semsoftware.editor;

import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.util.Map;

@ManagedBean(name = "editor")
public class EditorBean {

	private String value = "This editor is provided by PrimeFaces";

	public String getValue() {
		String test = getRequestParameterMap().get("test");
		return value + test;
	}

	public static Map<String, String> getRequestParameterMap() {
		FacesContext context = FacesContext.getCurrentInstance();
		if (context != null) {
			ExternalContext externalContext = context.getExternalContext();
			if (externalContext != null) {
				return externalContext.getRequestParameterMap();
			}
		}
		return null;
	}

	public void setValue(String value) {
		this.value = value;
	}
}