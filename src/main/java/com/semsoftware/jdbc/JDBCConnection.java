package com.semsoftware.jdbc;

/**
 * Created by Martina on 08.01.2016.
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnection {

    private static Connection connection;

    /**
     * Erzeugen eines neuen Connections
     */
    public JDBCConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            openConnection();
        } catch (ClassNotFoundException e) {
            System.out.println("ERROR: failed to load HSQLDB JDBC driver");
            e.printStackTrace();
            return;
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @throws SQLException Neue Connection wird aufgebaut, wenn es noch nicht zu dem Datenbank eine Verbindung besteht
     */
    public static void openConnection() throws SQLException {
        if (connection == null || isConnected() == false) {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/zumgoldenenloewen?autoReconnect=true", "monty", "seminar13");
        }

    }

    /**
     * Schliessen des Connections
     */
    public static void closeConnection() throws SQLException {
        if (isConnected()) {
            connection.close();
        }
    }

    /**
     * @return wird angezeigt ob einen Connection besteht oder nicht
     */
    public static boolean isConnected() {
        if (connection == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return Verbindung wird zurueckgegeben; wenn es noch nicht aufgebaut ist, muss esrst aufgebaut werden
     * @throws SQLException
     */
    public Connection getConnection() throws SQLException {
        if (isConnected()) {
            return connection;
        } else {
            openConnection();
            return getConnection();
        }
    }


}